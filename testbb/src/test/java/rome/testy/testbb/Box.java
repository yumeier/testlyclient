package rome.testy.testbb;

import org.osgi.service.component.annotations.Component;

@Component
public class Box {
	
	public String status() {
		return "open";
	}
}
